package com.songoda.epiclevels.command.commands;

import com.songoda.epiclevels.EpicLevels;
import com.songoda.epiclevels.command.AbstractCommand;
import com.songoda.epiclevels.gui.GUILevels;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class CommandEpicLevels extends AbstractCommand {

    public CommandEpicLevels() {
        super(null, true, "EpicLevels");
    }

    @Override
    protected ReturnType runCommand(EpicLevels instance, CommandSender sender, String... args) {
        new GUILevels(instance, (Player)sender, null);
        return ReturnType.SUCCESS;
    }

    @Override
    protected List<String> onTab(EpicLevels instance, CommandSender sender, String... args) {
        return null;
    }

    @Override
    public String getPermissionNode() {
        return "epiclevels.menu";
    }

    @Override
    public String getSyntax() {
        return "/levels";
    }

    @Override
    public String getDescription() {
        return "Displays top levels.";
    }
}
